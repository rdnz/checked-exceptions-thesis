{-# language TypeFamilies #-}

module Main where

import qualified Debug.Trace

-- { all
expensive :: (a ~ Bool) => a
expensive = Debug.Trace.trace "expensive computation" False

value0 :: Bool
value0 = expensive

value1 :: Bool
value1 = expensive

main :: IO ()
main =
  print value0 *>
  print value1
-- } all

{-
-- { core
expensive :: forall a. (a ~ Bool) => a
expensive (@a) ($d~1 :: a ~ Bool) =
  case eq_sel @(*) @a @Bool $d~1 of co
    _ ->
      (trace @Bool (unpackCString# "expensive computation"#) False)
        `cast` (Sub (Sym co) :: Bool ~R# a)

value0 :: Bool
value0 = expensive @Bool $d~0

value1 :: Bool
value1 = expensive @Bool $d~0

$d~0 :: Bool ~ Bool
$d~0 = Eq# @(*) @Bool @Bool @~(<Bool>_N :: Bool ~# Bool)
-- } core
-}

{-
-- { core_ill
expensive :: forall a. (a ~ Bool) => a
expensive (@a) ($d~1 :: a ~ Bool) =
  (trace @Bool (unpackCString# "expensive computation"#) False)
    `cast` (Sub (Sym (eq_sel @(*) @a @Bool $d~1)) :: Bool ~R# a)

value :: Char
value = expensive @Char $d~0

$d~0 :: Char ~ Bool
$d~0 = $d~0
-- } core_ill
-}
