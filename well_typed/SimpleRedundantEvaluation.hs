{-# OPTIONS_GHC -Wredundant-constraints #-}

module SimpleRedundantEvaluation where

import Library

import qualified Debug.Trace

data ExceptionA = ExceptionA deriving (Show)

-- { all
expensive ::
  (Elem ExceptionA exceptions) =>
  CatchChecked exceptions Char
expensive =
  Debug.Trace.trace "expensive computation" (throw ExceptionA)

handler :: ExceptionA -> CatchChecked void Char
handler ExceptionA = pure 'a'

expensiveReflexivity :: Bool
expensiveReflexivity =
  caughtAll (expensive `catch` handler)
  ==
  caughtAll (expensive `catch` handler)

main :: IO ()
main = print expensiveReflexivity
-- } all
