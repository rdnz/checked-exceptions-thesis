{-# language
  ExistentialQuantification,
  MultiParamTypeClasses,
  FlexibleInstances,
  TypeApplications,
  RankNTypes,
  AllowAmbiguousTypes,
  GeneralizedNewtypeDeriving,
  PolyKinds
#-}

module Library
  (
    Elem,
    CatchChecked (),
    throw,
    catch,
    caughtAll,
  )
where

import Data.Typeable (Typeable, cast)
import Unsafe.Coerce (unsafeCoerce)
import Data.Kind (Type)

data OpenSum (list :: Type) =
  forall value. Typeable value => OpenSum value

newtype CatchChecked list result =
  CatchChecked {unCatchChecked :: Either (OpenSum list) result}
  deriving (Functor, Applicative, Monad)

newtype Wrap element list value =
  Wrap {unWrap :: Elem element list => value}
newtype Catch element = Catch element

class X element
class X element => Elem element list

instance X (Catch element)
instance Elem (Catch element) list

uncheck ::
  forall (element :: Type) list value.
  (Elem element list => value) ->
  value
uncheck value =
  unWrap
    (
      unsafeCoerce (Wrap value :: Wrap element list value) ::
        Wrap (Catch element) list value
    )

-- { throw
throw ::
  (Elem exception list, Typeable exception) =>
  exception -> CatchChecked list result
-- } throw
throw = CatchChecked . Left . OpenSum

-- { catch
catch ::
  forall exception list result.
  (Typeable exception) =>
  (Elem exception list => CatchChecked list result) ->
  (exception -> CatchChecked list result) ->
  CatchChecked list result
-- } catch
catch value handler =
  either
    (openSumFoldUnchecked handler (CatchChecked . Left))
    (CatchChecked . Right) $
  unCatchChecked $
  (uncheck @exception @list value)

-- { caughtAll
caughtAll :: CatchChecked () result -> result
-- } caughtAll
caughtAll = either absurd id . unCatchChecked

openSumFoldUnchecked ::
  (Typeable element) =>
  (element -> result) ->
  (OpenSum list -> result) ->
  OpenSum list ->
  result
openSumFoldUnchecked f g openSum@(OpenSum value)
  | Just valueHead <- cast value = f valueHead
  | otherwise = g (OpenSum value)

absurd :: OpenSum () -> void
absurd = error "OpenSum's parameter list cannot be empty."
