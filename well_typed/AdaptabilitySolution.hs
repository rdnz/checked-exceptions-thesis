{-# language
  PartialTypeSignatures
#-}

{-# OPTIONS_GHC -Wredundant-constraints #-}

module AdaptabilitySolution where

import Library

import qualified Data.Char

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)
data ExceptionA' = ExceptionA' deriving (Show)

throwerA ::
  (Elem ExceptionA' exceptions) =>
  CatchChecked exceptions Char
throwerA = throw ExceptionA'

-- { throwerAB
throwerAB ::
  (
    Elem ExceptionB exceptions,
    _
  ) =>
  CatchChecked exceptions Char
throwerAB =
  do
    resultA <- throwerA
    if Data.Char.isAlpha resultA
      then pure 'c'
      else throw ExceptionB
-- } throwerAB

catcherA ::
  (Elem ExceptionB exceptions) =>
  CatchChecked exceptions Char
catcherA =
  throwerAB `catch` handler
  where
    handler ::
      (Elem ExceptionB exceptions) =>
      ExceptionA' -> CatchChecked exceptions Char
    handler ExceptionA' = throw ExceptionB

catcherB :: Char
catcherB =
  caughtAll (catcherA `catch` handler)
  where
    handler :: ExceptionB -> CatchChecked void Char
    handler ExceptionB = pure 'b'

main :: IO ()
main = print catcherB
-- } all
