{-# OPTIONS_GHC -Wredundant-constraints #-}

module SimpleUse where

import Library

import qualified Data.Char

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)

-- { all
throwerA ::
  (Elem ExceptionA exceptions) =>
  CatchChecked exceptions Char
throwerA = throw ExceptionA

throwerAB ::
  (
    Elem ExceptionA exceptions,
    Elem ExceptionB exceptions
  ) =>
  CatchChecked exceptions Char
throwerAB =
  do
    resultA <- throwerA
    if Data.Char.isAlpha resultA
      then pure 'c'
      else throw ExceptionB

catcherA ::
  (Elem ExceptionB exceptions) =>
  CatchChecked exceptions Char
catcherA =
  throwerAB `catch` handler
  where
    handler ::
      (Elem ExceptionB exceptions) =>
      ExceptionA -> CatchChecked exceptions Char
    handler ExceptionA = throw ExceptionB

catcherB :: Char
catcherB =
  caughtAll (catcherA `catch` handler)
  where
    handler :: ExceptionB -> CatchChecked void Char
    handler ExceptionB = pure 'b'

main :: IO ()
main = print catcherB
-- } all

{-
-- { imprecise
throwerA ::
  (
    Elem ExceptionA exceptions,
    Elem ExceptionB exceptions
  ) =>
  CatchChecked exceptions Char
throwerA = throw ExceptionA
-- } imprecise
-}
