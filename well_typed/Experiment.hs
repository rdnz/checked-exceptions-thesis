{-# language
  FlexibleContexts,
  PartialTypeSignatures,
  RankNTypes,
  ImpredicativeTypes
#-}

module Experiment where

import Library
import Data.Typeable (Typeable, cast)

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)

throwerA ::
  (Char, (Elem ExceptionA exceptions) => CatchChecked exceptions Bool)
throwerA = ('c', throw ExceptionA)

catchAndHandle ::
  (a, (Elem ExceptionA exceptions) => CatchChecked exceptions Bool) ->
  CatchChecked exceptions Bool
catchAndHandle (_, b) = b `catch` handler

catcherA :: Bool
catcherA =
  -- to-do. why does this not work while the next three work?
  -- caughtAll ((\x -> catchAndHandle x) throwerA)

  -- caughtAll (catchAndHandle throwerA)

  -- caughtAll
  --   (
  --     (
  --       (\x -> catchAndHandle x) ::
  --         (a, (Elem ExceptionA exceptions) => CatchChecked exceptions Bool) ->
  --         CatchChecked exceptions Bool
  --     )
  --       throwerA
  --   )

  caughtAll $
  case {- fmap (\x->x) -} throwerA of
    (_, b) -> b `catch` handler

  -- caughtAll (flip catch handler (snd throwerA))

handler :: ExceptionA -> CatchChecked void Bool
handler ExceptionA = pure False

main :: IO ()
main = print catcherA
