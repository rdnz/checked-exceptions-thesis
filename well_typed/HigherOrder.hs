{-# language
  ImpredicativeTypes
#-}

{-# OPTIONS_GHC -Wredundant-constraints #-}

module HigherOrder where

import Prelude hiding (traverse)

import Library

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)

throwerA ::
  (Elem ExceptionA exceptions) =>
  CatchChecked exceptions Bool
throwerA = throw ExceptionA

throwerAB ::
  (
    Elem ExceptionA exceptions,
    Elem ExceptionB exceptions
  ) =>
  CatchChecked exceptions Bool
throwerAB =
  do
    resultA <- throwerA
    if resultA
      then pure False
      else throw ExceptionB

listThrower ::
  [
    (Elem ExceptionA exceptions, Elem ExceptionB exceptions) =>
    CatchChecked exceptions Bool
  ]
listThrower = [throwerAB, throwerAB]

experiment ::
  (Elem ExceptionA exceptions, Elem ExceptionB exceptions) =>
  CatchChecked exceptions [Bool]
-- experiment = sequenceA (fmap (\x -> x) listThrower)
experiment = traverse listThrower

{-
But `traverse` cannot be implemented as specified because it cannot be
given a type. You might try something like
-}

traverse ::
  [(constraint, constraint') => CatchChecked exceptions result] ->
  ((constraint, constraint') => CatchChecked exceptions [result])
traverse (head : tail) = (:) <$> head <*> traverse tail
traverse [] = pure []

{-
and it works when a single exception is declared only, as I was amused
to discover. But for two possible exceptions you have to write
to-do. That is, type variables apparently only range over a single
constraint. Admittedly, I do not understand how this is consistent
with the fact that type synonyms of kind `Constraint` can represent
multiple constraints.
-}

main :: IO ()
main =
  print $
    caughtAll
      (expensive
        `catch` handlerB
        `catch` handlerA
      )
