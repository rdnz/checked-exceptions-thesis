{-# OPTIONS_GHC -Wredundant-constraints #-}

module ComplexRedundantEvaluation where

import Library

import qualified Debug.Trace
import qualified System.CPUTime

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)

-- { all
main :: IO ()
main =
  do
    timeStart <- System.CPUTime.getCPUTime
    print catcherAB
    timeMiddle <- System.CPUTime.getCPUTime
    print catcherBA
    timeEnd <- System.CPUTime.getCPUTime
    print $
      (fromInteger $ timeMiddle - timeStart) / (fromInteger $ 10 ^ 12)
    print $
      (fromInteger $ timeEnd - timeStart) / (fromInteger $ 10 ^ 12)

catcherAB :: Integer
catcherAB =
  caughtAll
    (expensive
      `catch` handlerA
      `catch` handlerB
    )

catcherBA :: Integer
catcherBA =
  caughtAll
    (expensive
      `catch` handlerB
      `catch` handlerA
    )

expensive ::
  (Elem ExceptionA exceptions) =>
  CatchChecked exceptions Integer
expensive = Debug.Trace.trace "expensive computation" (fib 32)

fib ::
  (Elem ExceptionA exceptions) =>
  Integer -> CatchChecked exceptions Integer
fib n
  | 0 <- n = pure 0
  | 1 <- n = pure 1
  | n > 1 = (+) <$> fib (n-1) <*> fib (n-2)
  | otherwise = throw ExceptionA

handlerA :: ExceptionA -> CatchChecked void Integer
handlerA ExceptionA = pure 0
handlerB :: ExceptionB -> CatchChecked void Integer
handlerB ExceptionB = pure 1
-- } all
