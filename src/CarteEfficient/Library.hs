{-# language
  TypeFamilies,
  DataKinds,
  ExistentialQuantification,
  PolyKinds,
  TypeOperators
#-}

-- { redundant_constraints
{-# OPTIONS_GHC -Wno-redundant-constraints #-}
-- } redundant_constraints

-- { exports
module CarteEfficient.Library
  (
    Elem,
    OpenSum (),
    makeOpenSum,
    openSumFold,
    absurd,
    throw,
    catch,
    caughtAll,
  )
where
-- } exports

-- { until_open_sum
import Data.Typeable (Typeable, cast)
import Data.Kind (Type)

type family Elem (element :: k) (list :: [k]) :: Type where
  Elem head (head ': _tail) = ()
  Elem element (_head ': tail) = Elem element tail

data OpenSum (list :: [Type]) =
  forall value. Typeable value => OpenSum value
-- } until_open_sum

-- { until_trusted_kernel_end
makeOpenSum ::
  (Elem element list ~ (), Typeable element) =>
  element -> OpenSum list
makeOpenSum = OpenSum

openSumFold ::
  (Typeable head) =>
  (head -> result) ->
  (OpenSum tail -> result) ->
  OpenSum (head ': tail) ->
  result
openSumFold f g openSum@(OpenSum value)
  | Just valueHead <- cast value = f valueHead
  | otherwise = g (OpenSum value)

absurd :: OpenSum '[] -> void
absurd = error "OpenSum's parameter list cannot be empty."
-- } until_trusted_kernel_end

-- { after_trusted_kernel
-- { throw
throw ::
  (Elem exception list ~ (), Typeable exception) =>
  exception -> Either (OpenSum list) result
-- } throw
throw = Left . makeOpenSum

-- { catch
catch ::
  (Typeable head) =>
  Either (OpenSum (head ': tail)) result ->
  (head -> Either (OpenSum tail) result) ->
  Either (OpenSum tail) result
-- } catch
catch value handler =
  either (openSumFold handler Left) Right value

-- { caughtAll
caughtAll :: Either (OpenSum '[]) result -> result
-- } caughtAll
caughtAll = either absurd id
-- } after_trusted_kernel
