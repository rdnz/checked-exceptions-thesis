{-# language
  TypeFamilies,
  PartialTypeSignatures,
  TypeApplications,
  DataKinds
#-}

{-# OPTIONS_GHC -Wredundant-constraints #-}

module CarteEfficient.Dismiss where

import CarteEfficient.Library

import qualified Data.Char

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)

throwerA ::
  -- (
  --   Elem ExceptionA exceptions ~ (),
  --   Elem ExceptionB exceptions ~ ()
  -- ) =>

  -- (_) =>

  (Elem ExceptionA exceptions ~ ()) =>
  Either (OpenSum exceptions) Char
throwerA = throw ExceptionA

throwerAB ::
  (
    -- Elem ExceptionA exceptions ~ (),
    Elem ExceptionB exceptions ~ (),
    _
  ) =>
  Either (OpenSum exceptions) Char
throwerAB =
  do
    resultA <- throwerA
    if Data.Char.isAlpha resultA
      then pure 'c'
      else throw ExceptionB

catcherAB :: Char
catcherAB =
  case throwerAB @'[ExceptionA, ExceptionB] of
    Left _ -> 'd'
    Right result -> result

main :: IO ()
main = print catcherAB
