{-# language
  TypeFamilies
#-}

{-# OPTIONS_GHC -Wredundant-constraints #-}

module CarteEfficient.SimpleUse where

import CarteEfficient.Library

import qualified Data.Char

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)

-- { all
throwerA ::
  (Elem ExceptionA exceptions ~ ()) =>
  Either (OpenSum exceptions) Char
throwerA = throw ExceptionA

throwerAB ::
  (
    Elem ExceptionA exceptions ~ (),
    Elem ExceptionB exceptions ~ ()
  ) =>
  Either (OpenSum exceptions) Char
throwerAB =
  do
    resultA <- throwerA
    if Data.Char.isAlpha resultA
      then pure 'c'
      else throw ExceptionB

catcherA ::
  (Elem ExceptionB exceptions ~ ()) =>
  Either (OpenSum exceptions) Char
catcherA =
  throwerAB `catch` handler
  where
    handler ::
      (Elem ExceptionB exceptions ~ ()) =>
      ExceptionA -> Either (OpenSum exceptions) Char
    handler ExceptionA = throw ExceptionB

catcherB :: Char
catcherB =
  caughtAll (catcherA `catch` handler)
  where
    handler :: ExceptionB -> Either (OpenSum void) Char
    handler ExceptionB = Right 'b'

main :: IO ()
main = print catcherB
-- } all

{-
-- { imprecise
throwerA ::
  (
    Elem ExceptionA exceptions ~ (),
    Elem ExceptionB exceptions ~ ()
  ) =>
  Either (OpenSum exceptions) Char
throwerA = throw ExceptionA
-- } imprecise
-}
