{-# language
  TypeFamilies
#-}

{-# OPTIONS_GHC -Wredundant-constraints #-}

module CarteEfficient.ComplexRedundantEvaluation where

import CarteEfficient.Library

import qualified Debug.Trace
import qualified System.CPUTime

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)

-- { all
main :: IO ()
main =
  do
    timeStart <- System.CPUTime.getCPUTime
    print catcherAB
    timeMiddle <- System.CPUTime.getCPUTime
    print catcherBA
    timeEnd <- System.CPUTime.getCPUTime
    print $
      (fromInteger $ timeMiddle - timeStart) / (fromInteger $ 10 ^ 12)
    print $
      (fromInteger $ timeEnd - timeStart) / (fromInteger $ 10 ^ 12)

catcherAB :: Integer
catcherAB =
  caughtAll
    (expensive
      `catch` handlerA
      `catch` handlerB
    )

catcherBA :: Integer
catcherBA =
  caughtAll
    (expensive
      `catch` handlerB
      `catch` handlerA
    )

expensive ::
  (Elem ExceptionA exceptions ~ ()) =>
  Either (OpenSum exceptions) Integer
expensive = Debug.Trace.trace "expensive computation" (fib 32)

fib ::
  (Elem ExceptionA exceptions ~ ()) =>
  Integer -> Either (OpenSum exceptions) Integer
fib n
  | 0 <- n = Right 0
  | 1 <- n = Right 1
  | n > 1 = (+) <$> fib (n-1) <*> fib (n-2)
  | otherwise = throw ExceptionA

handlerA :: ExceptionA -> Either void Integer
handlerA ExceptionA = Right 0
handlerB :: ExceptionB -> Either void Integer
handlerB ExceptionB = Right 1
-- } all
