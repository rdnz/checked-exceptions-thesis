{-# language
  TypeFamilies,
  PolyKinds,
  PartialTypeSignatures,
  TypeOperators,
  DataKinds,
  AllowAmbiguousTypes,
  RankNTypes,
  TypeApplications
#-}

{-# OPTIONS_GHC -Wredundant-constraints #-}

module CarteEfficient.Expressive where

import CarteEfficient.Library

import Data.Typeable (Typeable)
import qualified Data.Char

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)

throwButNotA ::
  (
    Typeable exception,
    Elem exception (ExceptionA ': tail) ~ ()
  ) =>
  exception -> Either (OpenSum tail) Char
throwButNotA exception = throw exception `catch` handler

handler :: ExceptionA -> Either (OpenSum void) Char
handler ExceptionA = pure 'a'

{-
'throwButNotA`'s type can be fully inferred, by the way. So it could
have been omitted or queried by writing `throwButNotA :: _ => _`
instead of the full type signature.

It might need some getting used to but the constraint `Elem exception
(ExceptionA ': tail) ~ ()` is equivalent to stating the logical
disjunction that the argument's type `exception` is equal to
`ExceptionA` or that the argument's type is element of the list `tail`
of escaping exceptions. Therefore, `throwButNotA`'s type expresses
almost precisely its semantics. "almost" because the mentioned
disjunction is inclusive, whereas exclusive disjunction would have
been precise. But this imprecision is insignificant as \ref{to-do}
demonstrates.
-}

-- { throwerA
throwerA ::
  (Elem ExceptionA exceptions ~ ()) =>
  Either (OpenSum exceptions) Char
throwerA = throw ExceptionA
-- } throwerA

throwerAB ::
  (
    Elem ExceptionA exceptions ~ (),
    Elem ExceptionB exceptions ~ ()
  ) =>
  Either (OpenSum exceptions) Char
throwerAB =
  do
    resultA <- throwerA
    if Data.Char.isAlpha resultA
      then pure 'c'
      else throw ExceptionB

finalizer ::
  (Elem ExceptionB tail ~ ()) =>
  (Bool, Either (OpenSum tail) Char)
finalizer =
  case (False, throwerAB) of
    (effect, Right result) -> (effect, Right result)
    (effect, Left openSum) ->
      openSumFold
        (\ExceptionA -> (effect, Right 'a'))
        (\openSumTail -> (True, Left openSumTail))
        openSum

type family Concat (list0 :: [k]) (list1 :: [k]) :: [k] where
  Concat '[] list1 = list1
  Concat (head0 ': tail0) list1 = head0 ': Concat tail0 list1

catchAB ::
  Either (OpenSum (Concat '[ExceptionA, ExceptionB] tail)) Char ->
  Either (OpenSum tail) Char
catchAB value =
  value `catch` handlerA `catch` handlerB
  where
    handlerA :: ExceptionA -> Either (OpenSum void) Char
    handlerA ExceptionA = pure 'a'
    handlerB :: ExceptionB -> Either (OpenSum void) Char
    handlerB ExceptionB = pure 'b'

catchPoly ::
  (
    forall tail.
    Either (OpenSum (Concat caughtExceptions tail)) result ->
    Either (OpenSum tail) result
  ) ->
  (
    Either (OpenSum (Concat caughtExceptions tail0)) result,
    Either (OpenSum (Concat caughtExceptions tail1)) result
  ) ->
  (Either (OpenSum tail0) result, Either (OpenSum tail1) result)
catchPoly catcher (expression0, expression1) =
  (catcher expression0, catcher expression1)

catchPolyExample0 :: (Char, Char)
catchPolyExample0 =
  bimap caughtAll caughtAll $
  (catchPoly @'[ExceptionA, ExceptionB]) catchAB $
  (throwerAB, throwerAB)

bimap :: (a -> b) -> (c -> d) -> (a, c) -> (b, d)
bimap f g (a, b) = (f a, g b)

catchPolyExample1 :: (Char, Char)
catchPolyExample1 =
  bimap
    (caughtAll . catchAB)
    (caughtAll . catchAB)
    (throwerAB, throwerAB)

main :: IO ()
main = pure ()
