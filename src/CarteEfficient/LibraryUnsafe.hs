{-# language
  TypeFamilies,
  DataKinds,
  ExistentialQuantification,
  PolyKinds,
  TypeOperators
#-}

{-# OPTIONS_GHC -Wno-redundant-constraints #-}

module CarteEfficient.LibraryUnsafe
  (
    Elem,
    OpenSum (),
    makeOpenSum,
    openSumFold,
    absurd,
    throw,
    catch,
    caughtAll,
  )
where

import Data.Typeable (Typeable, cast)
import Data.Kind (Type)

type family Elem (element :: k) (list :: [k]) :: Type where
  Elem head (head ': _tail) = ()
  Elem element (_head ': tail) = Elem element tail

data OpenSum (list :: [Type]) =
  forall value. Typeable value => OpenSum value

makeOpenSum ::
  (Typeable element) =>
  element -> OpenSum list
makeOpenSum = OpenSum

openSumFold ::
  (Typeable head) =>
  (head -> result) ->
  (OpenSum tail -> result) ->
  OpenSum (head ': tail) ->
  result
openSumFold f g openSum@(OpenSum value)
  | Just valueHead <- cast value = f valueHead
  | otherwise = g (OpenSum value)

absurd :: OpenSum '[] -> void
absurd = error "OpenSum's parameter list cannot be empty."

throw ::
  (Typeable exception) =>
  exception -> Either (OpenSum list) result
throw = Left . makeOpenSum

catch ::
  (Typeable head) =>
  Either (OpenSum (head ': tail)) result ->
  (head -> Either (OpenSum tail) result) ->
  Either (OpenSum tail) result
catch value handler =
  either (openSumFold handler Left) Right value

caughtAll :: Either (OpenSum '[]) result -> result
caughtAll = either absurd id
