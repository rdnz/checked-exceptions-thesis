{-# language
  TypeFamilies
#-}

{-# OPTIONS_GHC -Wredundant-constraints #-}

module CarteEfficient.Traverse where

import qualified Data.Char
import Prelude hiding (traverse)

import CarteEfficient.Library

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)

-- { traverse
traverse ::
  (a -> Either (OpenSum exceptions) b) ->
  [a] ->
  Either (OpenSum exceptions) [b]
traverse f [] = Right []
traverse f (headA : tailA) =
  case (f headA, traverse f tailA) of
    (Left exception, _) -> Left exception
    (_, Left exception) -> Left exception
    (Right headB, Right tailB) -> Right (headB : tailB)
-- } traverse

-- { example
throwerAB ::
  (
    Elem ExceptionA exceptions ~ (),
    Elem ExceptionB exceptions ~ ()
  ) =>
  Char -> Either (OpenSum exceptions) Char
throwerAB char
  | Data.Char.isAlpha char = Right (Data.Char.toUpper char)
  | Data.Char.isSpace char = throw ExceptionA
  | otherwise = throw ExceptionB

example ::
  (
    Elem ExceptionA exceptions ~ (),
    Elem ExceptionB exceptions ~ ()
  ) =>
  Either (OpenSum exceptions) [Char]
example =
  traverse throwerAB ['a', 'A']
-- } example

{-
-- { example_bad
example ::
  (
    Elem ExceptionA exceptions ~ ()
  ) =>
  Either (OpenSum exceptions) [Char]
example =
  traverse throwerAB ['a', 'A']
-- } example_bad
-}

exampleResult :: String
exampleResult =
  caughtAll $
    (show <$> example)
      `catch` handleA
      `catch` handleB
  where
    handleA :: ExceptionA -> Either void String
    handleA = Right. show
    handleB :: ExceptionB -> Either void String
    handleB = Right. show

main :: IO ()
main = putStrLn exampleResult
