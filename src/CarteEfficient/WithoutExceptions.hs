{-# language
  TypeFamilies,
  TypeOperators,
  DataKinds,
  TypeApplications
#-}

module CarteEfficient.WithoutExceptions where

import Data.Typeable (Typeable)
import Debug.Trace (trace)

import CarteEfficient.Library

value0 :: (Elem Char list ~ ()) => OpenSum list
value0 = makeOpenSum 'c'

value1 :: (Elem Bool list ~ ()) => OpenSum list
value1 = makeOpenSum False

value2 ::
  (Elem Bool list ~ (), Elem Char list ~ ()) => Bool -> OpenSum list
value2 bool = if bool then value0 else value1

openSumToEither ::
  (Typeable head) =>
  OpenSum (head ': tail) ->
  Either (OpenSum tail) head
openSumToEither = openSumFold Right Left

main :: IO ()
main =
  putStrLn $
    let
      expensive ::
        (Elem Bool list ~ (), Elem Char list ~ ()) => OpenSum list
      expensive = trace "expensive computation" (value2 True)
    in
      either
        (
          either
            (const "impossible empty")
            (show @Bool)
          .
          openSumToEither
        )
        (show @Char)
        (openSumToEither expensive)
