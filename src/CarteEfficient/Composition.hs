{-# language
  TypeFamilies
#-}

{-# OPTIONS_GHC -Wredundant-constraints #-}

module CarteEfficient.Composition where

import CarteEfficient.Library

import qualified Data.Char

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)

-- { <=<
(<=<) ::
  (b -> Either (OpenSum exceptions) c) ->
  (a -> Either (OpenSum exceptions) b) ->
  (a -> Either (OpenSum exceptions) c)
(<=<) f g a =
  case g a of
    Right result -> f result
    Left exception -> Left exception
-- } <=<

-- { example
throwerA ::
  (Elem ExceptionA exceptions ~ ()) =>
  Char -> Either (OpenSum exceptions) Char
throwerA argument =
  if Data.Char.isAlpha argument
    then pure 'a'
    else throw ExceptionA

throwerB ::
  (Elem ExceptionB exceptions ~ ()) =>
  Char -> Either (OpenSum exceptions) Char
throwerB argument =
  if Data.Char.isAlpha argument
    then pure 'b'
    else throw ExceptionB

example ::
  (
    Elem ExceptionA exceptions ~ (),
    Elem ExceptionB exceptions ~ ()
  ) =>
  Char -> Either (OpenSum exceptions) Char
example = throwerB <=< throwerA
-- } example

{-
-- { example_bad
example ::
  (Elem ExceptionA exceptions ~ ()) =>
  Char -> Either (OpenSum exceptions) Char
example = throwerB <=< throwerA
-- } example_bad
-}

exampleResult :: String
exampleResult =
  caughtAll $
    (show <$> example 'c')
      `catch` handleA
      `catch` handleB
  where
    handleA :: ExceptionA -> Either void String
    handleA = Right. show
    handleB :: ExceptionB -> Either void String
    handleB = Right . show

main :: IO ()
main = putStrLn exampleResult
