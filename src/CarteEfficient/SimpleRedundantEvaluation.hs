{-# language
  TypeFamilies
#-}

module CarteEfficient.SimpleRedundantEvaluation where

import CarteEfficient.Library

import qualified Debug.Trace

data ExceptionA = ExceptionA deriving (Show)

-- { all
expensive ::
  (Elem ExceptionA exceptions ~ ()) =>
  Either (OpenSum exceptions) Char
expensive =
  Debug.Trace.trace "expensive computation" (throw ExceptionA)

handler :: ExceptionA -> Either (OpenSum void) Char
handler ExceptionA = Right 'a'

expensiveReflexivity :: Bool
expensiveReflexivity =
  caughtAll (expensive `catch` handler)
  ==
  caughtAll (expensive `catch` handler)

main :: IO ()
main = print expensiveReflexivity
-- } all

{-
-- { core
expensive ::
  forall (exceptions :: [*]).
  (Elem ExceptionA exceptions ~ ()) =>
  Either (OpenSum exceptions) Char
expensive (@(exceptions :: [*])) ($d~1 :: Elem ExceptionA exceptions ~ ()) =
  trace
    @(Either (OpenSum exceptions) Char)
    (unpackCString# "expensive computation"#)
    (Right @(OpenSum exceptions) @Char (C# 'c'#))

expensiveReflexivity :: Bool
expensiveReflexivity =
  ==
    @Char
    $fEqChar
    (caughtAll
      @Char
      (catch
        @ExceptionA
        @'[]
        @Char
        ($dTypeable `cast` $d~2)
        (expensive @'[ExceptionA] $d~0)
        (handler @'[])
      )
    )
    @Char
    $fEqChar
    (caughtAll
      @Char
      (catch
        @ExceptionA
        @'[]
        @Char
        ($dTypeable `cast` $d~2)
        (expensive @'[ExceptionA] $d~0)
        (handler @'[])
      )
    )

$d~0 :: Elem ExceptionA '[ExceptionA] ~ ()
$d~0 =
  Eq# @(*) @() @() @~(<()>_N :: () ~# ())
  `cast`
  (
    ((~) <*>_N (Sym (D:R:Elem[0] <*>_N <ExceptionA>_N <'[]>_N)) <()>_N)_R
    ::
    (() ~ ()) ~R# (Elem ExceptionA '[ExceptionA] ~ ())
  )
-- } core
-}
