{-# language
  DataKinds,
  TypeOperators
#-}

{-# OPTIONS_GHC -Wredundant-constraints #-}

module CarteEfficient.Catcher where

import CarteEfficient.Library

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)

exceptionAToChar ::
  Either (OpenSum (ExceptionA ': exceptions)) Char ->
  Either (OpenSum exceptions) Char
exceptionAToChar value = value `catch` handler

handler :: ExceptionA -> Either void Char
handler ExceptionA = Right 'a'
