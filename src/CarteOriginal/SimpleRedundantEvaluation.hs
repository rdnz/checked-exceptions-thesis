{-# language
  FlexibleContexts
#-}

{-# OPTIONS_GHC -Wredundant-constraints #-}

module CarteOriginal.SimpleRedundantEvaluation where

import CarteOriginal.Library

import Debug.Trace (trace)
import Data.Kind (Type)

data ExceptionA = ExceptionA deriving (Show)

thrower ::
  (Elem ExceptionA exceptions) =>
  Either exceptions Char
thrower = trace "expensive computation" (throw ExceptionA)

handler :: ExceptionA -> Either void Char
handler ExceptionA = Right 'a'

main :: IO ()
main =
  print (caughtAll (thrower `catch` handler)) *>
  print (caughtAll (thrower `catch` handler)) *>
  print (caughtAll (thrower `catch` handler))
