{-# language
  MultiParamTypeClasses,
  FlexibleInstances
#-}

module CarteOriginal.Library where

-- { all
data OpenSum head tail = Head head | Tail tail

class Elem element openSum where
  makeOpenSum :: element -> openSum

instance {-# overlapping #-} Elem element (OpenSum element _tail)
  where
    makeOpenSum = Head

instance {-# overlappable #-}
  Elem element tail => Elem element (OpenSum _head tail)
  where
    makeOpenSum = Tail . makeOpenSum

openSumFold ::
  (head -> result) ->
  (tail -> result) ->
  OpenSum head tail ->
  result
openSumFold f g (Head value) = f value
openSumFold f g (Tail value) = g value

-- { throw
throw ::
  (Elem exception openSum) =>
  exception -> Either openSum result
-- } throw
throw = Left . makeOpenSum

-- { catch
catch ::
  Either (OpenSum head tail) result ->
  (head -> Either tail result) ->
  Either tail result
-- } catch
catch value handler =
  either (openSumFold handler Left) Right value

-- { caughtAll
caughtAll :: Either result result -> result
-- } caughtAll
caughtAll = either id id
-- } all
