{-# language
  PartialTypeSignatures,
  FlexibleContexts
#-}

{-# OPTIONS_GHC -Wredundant-constraints #-}

module CarteOriginal.SimpleUse where

import CarteOriginal.Library

import qualified Data.Char

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)

-- { all
throwerA ::
  (Elem ExceptionA exceptions) =>
  Either exceptions Char
throwerA = throw ExceptionA

throwerAB ::
  (
    Elem ExceptionA exceptions,
    Elem ExceptionB exceptions
  ) =>
  Either exceptions Char
throwerAB =
  do
    resultA <- throwerA
    if Data.Char.isAlpha resultA
      then pure 'c'
      else throw ExceptionB

catcherA ::
  (Elem ExceptionB exceptions) =>
  Either exceptions Char
catcherA =
  throwerAB `catch` handler
  where
    handler ::
      (Elem ExceptionB exceptions) =>
      ExceptionA -> Either exceptions Char
    handler ExceptionA = throw ExceptionB

catcherB :: Char
catcherB =
  caughtAll (catcherA `catch` handler)
  where
    handler :: ExceptionB -> Either void Char
    handler ExceptionB = Right 'b'

main :: IO ()
main = print catcherB
-- } all
