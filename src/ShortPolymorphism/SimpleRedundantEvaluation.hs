{-# language
  DataKinds,
  FlexibleContexts
#-}

{-# OPTIONS_GHC -Wno-simplifiable-class-constraints #-}

module ShortPolymorphism.SimpleRedundantEvaluation where

import ShortPolymorphism.Library

import qualified Debug.Trace

data ExceptionA = ExceptionA deriving (Show)

-- { all
expensive ::
  (Subset '[ExceptionA] exceptions) =>
  Either (OpenSum exceptions) Char
expensive =
  Debug.Trace.trace "expensive computation" $
    weakenExceptions (throw ExceptionA)

handler :: ExceptionA -> Either (OpenSum void) Char
handler ExceptionA = Right 'a'

expensiveReflexivity :: Bool
expensiveReflexivity =
  caughtAll
    ((expensive :: Either (OpenSum '[ExceptionA]) Char)
      `catch` handler
    )
  ==
  caughtAll
    ((expensive :: Either (OpenSum '[ExceptionA]) Char)
      `catch` handler
    )

main :: IO ()
main = print expensiveReflexivity
-- } all
