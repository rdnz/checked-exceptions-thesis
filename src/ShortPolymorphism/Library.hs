{-# language
  DataKinds,
  TypeOperators,
  PolyKinds
#-}

module ShortPolymorphism.Library
  (
    module ShortPolymorphism.Library,
    module Common.OpenSumWithoutTypeable,
  )
where

import Common.OpenSumWithoutTypeable hiding (OpenSum (..))
import Common.OpenSumWithoutTypeable (OpenSum ())

-- { all
-- { >>=*
(>>=*) ::
  (list0 `Subset` list2, list1 `Subset` list2) =>
  Either (OpenSum list0) a ->
  (a -> Either (OpenSum list1) b) ->
  Either (OpenSum list2) b
-- } >>=*
a >>=* f = weakenExceptions a >>= weakenExceptions . f
-- } all
