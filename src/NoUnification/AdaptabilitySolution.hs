{-# language
  DataKinds,
  FlexibleContexts,
  PartialTypeSignatures
#-}

module NoUnification.AdaptabilitySolution where

import NoUnification.Library

import qualified Data.Char

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)
data ExceptionA' = ExceptionA' deriving (Show)

throwerA :: Either (OpenSum '[ExceptionA']) Char
throwerA = throw ExceptionA'

-- { throwerAB
throwerAB ::
  Either (OpenSum _) Char
throwerAB =
  throwerA >>=* \resultA ->
  if Data.Char.isAlpha resultA
    then pure 'c'
    else throw ExceptionB
-- } throwerAB

catcherA :: Either (OpenSum '[ExceptionB]) Char
catcherA =
  throwerAB `catch` handler
  where
    handler :: ExceptionA' -> Either (OpenSum '[ExceptionB]) Char
    handler ExceptionA' = throw ExceptionB

catcherB :: Char
catcherB =
  caughtAll (catcherA `catch` handler)
  where
    handler :: ExceptionB -> Either void Char
    handler ExceptionB = Right 'b'

main :: IO ()
main = print catcherB
