{-# language
  DataKinds
#-}

module NoUnification.SimpleUse where

import NoUnification.Library

import qualified Data.Char

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)

-- { all
throwerA :: Either (OpenSum '[ExceptionA]) Char
throwerA = throw ExceptionA

throwerAB :: Either (OpenSum '[ExceptionB, ExceptionA]) Char
throwerAB =
  exceptionSet $
    throwerA >>=* \resultA ->
    if Data.Char.isAlpha resultA
      then pure 'c'
      else throw ExceptionB

catcherA :: Either (OpenSum '[ExceptionB]) Char
catcherA =
  throwerAB `catch` handler
  where
    handler :: ExceptionA -> Either (OpenSum '[ExceptionB]) Char
    handler ExceptionA = throw ExceptionB

catcherB :: Char
catcherB =
  caughtAll (catcherA `catch` handler)
  where
    handler :: ExceptionB -> Either void Char
    handler ExceptionB = Right 'b'

main :: IO ()
main = print catcherB
-- } all

{-
-- { imprecise
throwerA :: Either (OpenSum '[ExceptionA, ExceptionB]) Char
throwerA = throw ExceptionA
-- } imprecise
-}
