{-# language
  DataKinds
#-}

module NoUnification.Traverse where

import NoUnification.Library

import qualified Data.Char
import Prelude hiding (traverse)

data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)

-- { traverse
traverse ::
  (a -> Either (OpenSum exceptions) b) ->
  [a] ->
  Either (OpenSum exceptions) [b]
traverse f [] = Right []
traverse f (headA : tailA) =
  case (f headA, traverse f tailA) of
    (Left exception, _) -> Left exception
    (_, Left exception) -> Left exception
    (Right headB, Right tailB) -> Right (headB : tailB)
-- } traverse

-- { example
throwerA :: Char -> Either (OpenSum '[ExceptionA]) Char
throwerA char
  | Data.Char.isAlpha char = Right (Data.Char.toUpper char)
  | otherwise = throw ExceptionA

example :: Either (OpenSum '[ExceptionA]) [Char]
example =
  traverse throwerA ['a', 'A']
-- } example

{-
-- { example_bad
example :: Either (OpenSum '[]) [Char]
example =
  traverse throwerA ['a', 'A']
-- } example_bad
-}

exampleResult :: String
exampleResult =
  caughtAll $ (show <$> example) `catch` handleA
  where
    handleA :: ExceptionA -> Either void String
    handleA = Right . show

main :: IO ()
main = putStrLn exampleResult
