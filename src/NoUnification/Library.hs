{-# language
  PolyKinds,
  DataKinds,
  TypeOperators,
  TypeFamilies,
  FlexibleContexts
#-}

module NoUnification.Library
  (
    module NoUnification.Library,
    module Common.OpenSumWithoutTypeable,
  )
where

import Common.OpenSumWithoutTypeable hiding (OpenSum (..))
import Common.OpenSumWithoutTypeable (OpenSum ())

type family Concat (list0 :: [k]) (list1 :: [k]) :: [k] where
  Concat '[] list1 = list1
  Concat (head0 ': tail0) list1 = head0 ': Concat tail0 list1

-- { exceptionSet
exceptionSet ::
  (Subset list0 list1, Subset list1 list0) =>
  Either (OpenSum list0) a ->
  Either (OpenSum list1) a
-- } exceptionSet
exceptionSet = weakenExceptions

-- { >>=*
(>>=*) ::
  (
    Subset list0 (Concat list0 list1),
    Subset list1 (Concat list0 list1)
  ) =>
  Either (OpenSum list0) a ->
  (a -> Either (OpenSum list1) b) ->
  Either (OpenSum (Concat list0 list1)) b
-- } >>=*
Right a >>=* f = either (Left . weaken) Right (f a)
Left openSum >>=* _f = Left (weaken openSum)
