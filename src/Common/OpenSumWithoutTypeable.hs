{-# language
  TypeFamilies,
  DataKinds,
  ExistentialQuantification,
  PolyKinds,
  TypeOperators,
  TypeApplications,
  ScopedTypeVariables,
  AllowAmbiguousTypes,
  MultiParamTypeClasses,
  FlexibleInstances,
  FlexibleContexts
#-}

module Common.OpenSumWithoutTypeable where

-- { all
import Unsafe.Coerce (unsafeCoerce)
import Data.Kind (Constraint, Type)

type family Delete (element :: k) (list :: [k]) :: [k] where
  Delete _element '[] = '[]
  Delete head (head ': tail) = Delete head tail
  Delete element (head ': tail) = head ': Delete element tail

class Elem (element :: k) (list :: [k]) where
  elemIndexPromoted :: Int

instance {-# overlapping #-} Elem head (head ': _tail) where
  elemIndexPromoted = 0

instance {-# overlappable #-}
  Elem element tail => Elem element (_head ': tail)
  where
    elemIndexPromoted = 1 + elemIndexPromoted @_ @element @tail

class ElemIndices (element :: k) (list :: [k]) where
  elemIndices :: [Int]

instance {-# overlapping #-} ElemIndices _element '[] where
  elemIndices = []

instance {-# overlapping #-}
  ElemIndices head tail =>
    ElemIndices head (head ': tail)
  where
    elemIndices = 0 : fmap succ (elemIndices @_ @head @tail)

instance {-# overlappable #-}
  ElemIndices element tail =>
    ElemIndices element (head ': tail)
  where
    elemIndices = fmap succ (elemIndices @_ @element @tail)

class Subset (list0 :: [k]) (list1 :: [k]) where
  indexEmbedding :: [Int]

instance Subset '[] _list1 where
  indexEmbedding = []

instance
  (Elem head0 list1, Subset tail0 list1) =>
    Subset (head0 ': tail0) list1
  where
    indexEmbedding =
      elemIndexPromoted @_ @head0 @list1
      :
      indexEmbedding @_ @tail0 @list1

data OpenSum (list :: [Type]) = forall value. OpenSum Int value

makeOpenSum :: element -> OpenSum '[element]
makeOpenSum = OpenSum 0

openSumFold ::
  forall element list result.
  (ElemIndices element list) =>
  (element -> result) ->
  (OpenSum (Delete element list) -> result) ->
  OpenSum list ->
  result
openSumFold f g (OpenSum index value)
  | elem index (elemIndices @_ @element @list) = f (unsafeCoerce value)
  | otherwise = g (OpenSum (index - indexOffset) value)
  where
    indexOffset :: Int
    indexOffset =
      length $
      takeWhile (< index) $
      elemIndices @_ @element @list

absurd :: OpenSum '[] -> void
absurd = error "OpenSum's parameter list cannot be empty."

weaken ::
  forall list0 list1.
  (Subset list0 list1) => OpenSum list0 -> OpenSum list1
weaken (OpenSum index value) =
  OpenSum
    (indexEmbedding @_ @list0 @list1 !! index)
    value

-- { throw
throw :: exception -> Either (OpenSum '[exception]) result
-- } throw
throw = Left . makeOpenSum

-- { catch
catch ::
  (ElemIndices exception list) =>
  Either (OpenSum list) result ->
  (exception -> Either (OpenSum (Delete exception list)) result) ->
  Either (OpenSum (Delete exception list)) result
-- } catch
catch value handler =
  either
    (openSumFold handler Left)
    Right
    value

-- { caughtAll
caughtAll :: Either (OpenSum '[]) result -> result
-- } caughtAll
caughtAll = either absurd id

-- { weakenExceptions
weakenExceptions ::
  (Subset list0 list1) =>
  Either (OpenSum list0) a ->
  Either (OpenSum list1) a
-- } weakenExceptions
weakenExceptions = either (Left . weaken) Right
-- } all
