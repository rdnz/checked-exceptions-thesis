{-# language
  TypeOperators,
  DataKinds,
  ScopedTypeVariables,
  TypeApplications,
  PolyKinds
#-}

module CarteWithoutTypeable.Library
  (
    module CarteWithoutTypeable.Library,
    module Common.OpenSumWithoutTypeable
  )
where

import Common.OpenSumWithoutTypeable hiding
  (OpenSum (..), makeOpenSum, openSumFold, throw, catch)
import Common.OpenSumWithoutTypeable (OpenSum ())
import qualified Common.OpenSumWithoutTypeable as OS

import Unsafe.Coerce (unsafeCoerce)

makeOpenSum ::
  forall element list.
  (Elem element list) => element -> OpenSum list
makeOpenSum = OS.OpenSum (elemIndexPromoted @_ @element @list)

openSumFold ::
  (head -> result) ->
  (OpenSum tail -> result) ->
  OpenSum (head ': tail) ->
  result
openSumFold f g (OS.OpenSum index value)
  | index == 0 = f (unsafeCoerce value)
  | otherwise = g (OS.OpenSum (index - 1) value)

-- { throw
throw ::
  (Elem exception list) =>
  exception -> Either (OpenSum list) result
-- } throw
throw = Left . makeOpenSum

-- { catch
catch ::
  Either (OpenSum (head ': tail)) result ->
  (head -> Either (OpenSum tail) result) ->
  Either (OpenSum tail) result
-- } catch
catch value handler =
  either (openSumFold handler Left) Right value
