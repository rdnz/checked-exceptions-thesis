{-# language
  FlexibleContexts,
  PartialTypeSignatures
#-}

{-# OPTIONS_GHC -Wredundant-constraints #-}

module CarteWithoutTypeable.AdaptabilitySolution where

import CarteWithoutTypeable.Library

import qualified Data.Char

-- { exception_types
data ExceptionA = ExceptionA deriving (Show)
data ExceptionB = ExceptionB deriving (Show)
data ExceptionA' = ExceptionA' deriving (Show)
-- } exception_types

throwerA ::
  (Elem ExceptionA' exceptions) =>
  Either (OpenSum exceptions) Char
throwerA = throw ExceptionA'

-- { throwerAB
throwerAB ::
  (
    Elem ExceptionB exceptions,
    _
  ) =>
  Either (OpenSum exceptions) Char
throwerAB =
  do
    resultA <- throwerA
    if Data.Char.isAlpha resultA
      then pure 'c'
      else throw ExceptionB
-- } throwerAB

catcherA ::
  (Elem ExceptionB exceptions) =>
  Either (OpenSum exceptions) Char
catcherA =
  throwerAB `catch` handler
  where
    handler ::
      (Elem ExceptionB exceptions) =>
      ExceptionA' -> Either (OpenSum exceptions) Char
    handler ExceptionA' = throw ExceptionB

catcherB :: Char
catcherB =
  caughtAll (catcherA `catch` handler)
  where
    handler :: ExceptionB -> Either (OpenSum void) Char
    handler ExceptionB = Right 'b'

main :: IO ()
main = print catcherB
