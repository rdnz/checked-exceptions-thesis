{-# language
  FlexibleContexts
#-}

module CarteWithoutTypeable.SimpleRedundantEvaluation where

import CarteWithoutTypeable.Library

import qualified Debug.Trace

data ExceptionA = ExceptionA deriving (Show)

-- { all
expensive ::
  (Elem ExceptionA exceptions) =>
  Either (OpenSum exceptions) Char
expensive =
  Debug.Trace.trace "expensive computation" (throw ExceptionA)

handler :: ExceptionA -> Either (OpenSum void) Char
handler ExceptionA = Right 'a'

expensiveReflexivity :: Bool
expensiveReflexivity =
  caughtAll (expensive `catch` handler)
  ==
  caughtAll (expensive `catch` handler)

main :: IO ()
main = print expensiveReflexivity
-- } all
