module Motivation where

-- { computation
import Control.Monad.Catch (SomeException, throwM, catch)

computation ::
  Either SomeException Result -> Either SomeException Result
computation = step2 . step1
-- } computation

-- { steps
step1 :: Either SomeException Result -> Either SomeException Result
step1 argument =
  if someComputation
  then throwM Step1Exception
  else someOtherComputation

step2 :: Either SomeException Result -> Either SomeException Result
step2 argument =
  if someImportComputation
  then argument `catch` (\Step1Exception -> someRecovery)
  else someMoreComputation
-- } steps
